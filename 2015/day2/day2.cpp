#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

struct Present {
    int length, width, height;
};

Present *make_presents(string filename, int &present_count);
int paper_quantity(Present *presents, int &present_count);
int calculate_sqft(int length, int width, int height);
int smallest_sqft(int length, int width, int height);
int ribon_qty(Present *presents, int &present_count);

int main(int argc, char *argv[]) {
    int present_count = 0;
    Present *presents = make_presents(argv[1], present_count);

    // new function for calculating surface area and present area
    cout << "Part One: " << paper_quantity(presents, present_count) << " sqft" << endl;
    cout << "Part Two: " << ribon_qty(presents, present_count) << endl;
    delete [] presents;
    return 0;
}

Present *make_presents(string filename, int &present_count) {
    ifstream infile(filename);
    string tmp_line;
    infile >> tmp_line;
    while (!infile.eof()) {
        present_count += 1;
        infile >> tmp_line;
    }
    infile.close();

    Present *presents = new Present[present_count];

    ifstream infile2(filename);
    string next_line;
    int x_count = 0;
    infile2 >> next_line;
    while (!infile2.eof()) {
        string tmp_string;

        for (int i = 0; i < present_count; i++) {
            tmp_string = "";
            x_count = 0;
            //cout << "i: " << i << endl;
            //cout << next_line << endl;
            for (char next_character : next_line) {
                //cout << next_character << endl;
                if (next_character == 'x' and x_count == 0) {
                    presents[i].length = stoi(tmp_string);
                    tmp_string = "";
                    //cout << presents[i].length << " ";
                    x_count += 1;
                    continue;
                }
                else if (next_character == 'x' and x_count == 1) {
                    presents[i].width = stoi(tmp_string);
                    tmp_string = "";
                    //cout << presents[i].width << " ";
                    x_count += 1;
                    continue;
                }
                else {
                    //cout << next_character << endl;
                    tmp_string.push_back(next_character);
                    //cout << tmp_string << endl;
                }
                if (x_count == 2) {
                    presents[i].height = stoi(tmp_string);
                    //cout << presents[i].height << endl;
                    continue;
                }
            }
            infile2 >> next_line;
        }
    }
    return presents;
}


int paper_quantity(Present *presents, int &present_count) {
    int total_quantity = 0;
    for (int i = 0; i < present_count; i++) {
        total_quantity += calculate_sqft(presents[i].length, presents[i].width, presents[i].height) + smallest_sqft(presents[i].length, presents[i].width, presents[i].height);
    }
    return total_quantity;
}

int calculate_sqft(int length, int width, int height) {
    return (2*length*width) + (2*width*height) +(2*height*length);
}

int smallest_sqft(int length, int width, int height) {
    int areas[3];
    areas[0] = (length*width);
    areas[1] = (height*width);
    areas[2] = (length*height);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (areas[i] < areas[j]) {
                int tmp;
                tmp = areas[j];
                areas[j] = areas[i];
                areas[i] = tmp;
            }
        }
    }
    return areas[0];
}

int smallest_perimeter(Present present) {
    int lengths[3];
    lengths[0] = present.length;
    lengths[1] = present.width;
    lengths[2] = present.height;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (lengths[i] < lengths[j]) {
                int tmp;
                tmp = lengths[j];
                lengths[j] = lengths[i];
                lengths[i] = tmp;
            }
        }
    }
    return (2*lengths[0]) + (2*lengths[1]);
}

int ribon_qty(Present *presents, int &present_count) {
    int total_quantity = 0;
    for (int i = 0; i < present_count; i++) {
        total_quantity += smallest_perimeter(presents[i]) + (presents[i].length*presents[i].width*presents[i].height);
    }
    return total_quantity;
}
