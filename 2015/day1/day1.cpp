#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

int part_one (string filename);
int part_two (string filename);

int main(int argc, char *argv[]) {
    cout << "Part One: Floor " << part_one(argv[1]) << endl;
    cout << "Part Two: Index " << part_two(argv[1]) << endl;

    return 0;
}


int part_one (string filename) {
    ifstream infile(filename);
    char next_character;
    int open_count = 0;
    int close_count = 0;
    infile >> next_character;
    while (!infile.eof()) {
        if (next_character == '(') {
            open_count += 1;
        }
        if (next_character == ')') {
            close_count += 1;
        }
        infile >> next_character;
    }
    infile.close();
    return open_count - close_count;
}

int part_two (string filename) {
    ifstream infile(filename);
    int file_length = 0;
    char tmp;
    infile >> tmp;
    while (!infile.eof()) {
        file_length += 1;
        infile >> tmp;
    }
    infile.close();
    char *input = new char[file_length];

    ifstream infile2(filename);
    char next_character;
    infile2 >> next_character;
    for (int i = 0; i < file_length; i++) {
        input[i] = next_character;
        infile2 >> next_character;
    }
    infile2.close();

    int open_count = 0;
    int close_count = 0;
    for (int i = 0; i < file_length; i++) {
        if (input[i] == '(') {
            open_count += 1;
        }
        if (input[i] == ')') {
            close_count += 1;
        }
        if ((open_count - close_count) == -1) {
            return i + 1;
        }
    }

    delete [] input;
    return file_length;
}
