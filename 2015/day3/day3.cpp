// day3.cpp
// Author: cole-glotfelty
// Date: 2023-04-12
// Purpose: to solve day 3 of 2015 advent of code

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <map>

using namespace std;

struct Position {
    int x, y;
};

bool operator<(const Position &p1, const Position &p2) {
    if (p1.x < p2.x)
        return true;
    if (p1.x > p2.x)
        return false;
    return p1.y < p2.y;
}

map<Position, int> create_map(string filename);

int main(int argc, char *argv[]) {
    map<Position, int> position_map = create_map(argv[1]);
    return 0;
}

// coordinates, and present count

// start at 0,0
// take in command from file
// change coordinates, store in data sctructure, and update the count at the
// address

map<Position, int> create_map(string filename) {
    cout << filename;
    map<Position, int> position_map;

    return position_map;
}
